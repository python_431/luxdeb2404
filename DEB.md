## Commandes de gestion de paquets

- Le format DEB de distribution de paquets : fichier .deb
- Ils sont disponibles dans des dépôts (repositories)
  - debian.org
  - dépôts tiers (attention !)

Commandes de bases :

- `dpkg` : agit sur les fichier .deb et sur la base de données des
   paquetages installés (`/var/lib/dpkg`)
- Historiquement : `apt-get/apt-cache` remplacé par `apt`
- En option : `apt-file` recherche de fichiers dans les paquets 
  disponible
- Configuration des dépôts : `/etc/apt/sources.list` et répertoire
  `/etc/apt/sources.list.d/*.list` 

~~~~Bash
$ sudo apt update # met à jour la liste des paquets disponibles
$ sudo apt install paquet [paquet ...]
$ sudo apt upgrade # mise à jour
$ sudo apt full-upgrade
$ sudo apt dist-upgrade # après changement de nom de code dans sources.list
~~~~

Exemples avec `dpkg` :

~~~~Bash
$ dpkg -l
$ dpkg --purge ...
...
~~~~

Installation d'apt-file :

~~~~Bash
$ sudo apt install apt-file
$ sudo apt update 
$ apt-file fichier ...
~~~~

## Gestion des signatures de paquets

Les paquet Debian sont signé par une clef privée du projet Debian.

Lors de l'activation de dépôts tiers, il faut ajouter une clef publique
correspondante ce qui va permettre de valider l'authenticité des
paquets.

Exemple : PostgreSQL, Docker-CE, etc.

Ajouter un dépôt tiers et importer une clef publique c'est faire
confiance à :

- L'honnêteté des gestionnaires du dépôt
- La bonne gestion de la sécurité 
- Les gardes-fous :
   - URL du dépôt (sécu du dns, et du serveur)
   - Clef privée est gardé secrète

Example : postgreSQL

http://www.postgresql.org/

En suivant la procédure officielle, tout en l'améliorant :

~~~~Bash
$ echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list
$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
~~~~

Avertissement : c'est plus la bonne méthode. On enlève cette clef :
~~~~Bash
$ apt-key list
$ sudo apt-key del "B97B 0AFC AA1A 47F0 44F2  44A0 7FCC 7D46 ACCC 4CF8"
~~~~

Cf. https://www.numetopia.fr/comment-corriger-lavertissement-apt-key-is-deprecated/

On commence par mettre la clef dans un répertoire dédié (`/usr/share/keyring`)

~~~~Bash
wget -O- https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo gpg --dearmor --yes --output /usr/share/keyrings/pgdg-archive-keyring.gpg
~~~~

Convertit du format ascii au format binaire et place la clef dans /usr/share/keyring, il faut alors adapter le fichier `/etc/sources.list.d/pgdb.list` pour y
mentionner cette clef :

~~~~
deb [signed-by=/usr/share/keyrings/pgdg-archive-keyring.gpg] https://apt.postgresql.org/pub/repos/apt bookworm-pgdg main
~~~~

On teste : 

~~~~Bash
$ apt update
$ apt install postgresql-16
~~~~

## Construire un paquet à partir des sources

On peut télécharger les sources d'un paquet existant :

~~~~Bash
$ mkdir dev
$ cd dev
$ apt source apache2
$ cd apache...
~~~~

Fichiers les plus importants sont `debian/rules` et `debian/changelog`

