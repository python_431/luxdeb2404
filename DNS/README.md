# DNS et Bind

Le DNS _(Domain Name System)_ est une base de données distribuée qui
permet (entre autres) d'obtenir des IPs à partir de noms.

Base de données hiérarchique :

~~~~
                       .
                       |
  ------------------------------------------------------
  |    |    |     |           |            |           |
 fr   uk   com   org  ...   museum       sncf         bzh
            |
           truc
            |
           www
~~~~


## Installer et configurer Bind

~~~~Bash
$ sudo apt install bind9 bind9-utils bind9-host
~~~~

On souhaite être client de notre propre dns tout en ayant une IP obtenu
par DHCP :

~~~~Bash
$ sudo nmcli connection modify Wired\ connection\ 1 ipv4.ignore-auto-dns yes
~~~~

On peut éditer  `/etc/resolv.conf` :

~~~~
nameserver 127.0.0.1
~~~~

Configuration de cinq domaines sous un TLD .insee :

- london
- sydney
- vertou
- lille
- rio

Il faut commencer par le proclamer dans named.conf.local :

~~~~
zone "rio.insee" {
   type master;
   file "/etc/bind/zones/db.rio.insee";
};
~~~~

Note : mon serveur "usurpe" le TLP insee. et il faut que vous m'utilisiez
comme forwarder et désactiviez dsn-sec (dans named.conf.options)

~~~~
        dnssec-validation no;
        dnssec-enable no;
~~~~


## Zone inverse : IP vers noms...

~~~~
                      .
                      |
   -----------------------------------------------------
   |     |            |
   fr   com          arpa 
                      |
                 --------------------------------
                 |                              |
              in-addr                         ip6
                 |                              |
               194   -> RIPE                    2
                 |                              |
               254    -> RENATER                0
                 |                             ...
                 37   -> INSEE                  |
                 |                              0
            -------------------                 |
            |                 |                 f [ 2001:0db8:...:1a0f]
          207                163                PTR name.domain...
  PTR w207.insee.fr     PTR w163.insee.fr
~~~~

Le nom DOIT exiter dans la zone directe du domine correspondant.


