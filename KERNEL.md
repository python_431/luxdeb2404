# Compilation d'un noyau Linux à partir des sources

- Télécharger les sources du noyau 6.8.7 sur kernel.org

~~~~Bash
$ tar xf lilnux-6.8.7.tar.xz
$ cd linux-6.8.7
$ cp /boot/config-$(uname -r) .config
$ make menuconfig # utilise libncurses
ou
$ make gconfig # Gtk
ou 
$ make xconfig # Qt : on prend celui-là
$ sudo apt install qt6-base-dev
$ make xconfig
~~~~

Important : ne pas tenter de signer le noyau et ses modules avec
la clef privée de Debian !

~~~~Bash
$ scripts/config --disable SYSTEM_TRUSTED_KEYS
$ scripts/config --disable SYSTEM_REVOCATION_KEYS
~~~~

Dépendance de compilation : 

~~~~Bash
$ sudo apt-get install build-essential bc kmod cpio flex libncurses5-dev libelf-dev libssl-dev dwarves bison
$ sudo apt install debhelper-compat
~~~~

Lancer la compilation :

~~~~Bash
$ make -j4 bindeb-pkg
~~~~

Si question sur X509 : [entrée]

