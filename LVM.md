# LVM : Logical Volume Manager

## Préparation des volumes physiques

_Note_ : avec l'hyperviseur Virtual Box on ne peut plus (on
pouvait en version 6) ajouter des disques SATA à chaud (sans
éteindre la VM). Du coup on éteint notre VM : `sudo poweroff`.

Configuration/Stockage/Contrôlleur SATA : ajouter trois disques
de 20Gio.

Ajouter un disque dur/Créer/Valeurs par défaut/Choose, à faire
trois fois.

Une fois démarrée on voit nos disques dans la VM :

~~~~Bash
$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   20G  0 disk 
├─sda1   8:1    0  512M  0 part /boot/efi
├─sda2   8:2    0 18,5G  0 part /
└─sda3   8:3    0  976M  0 part [SWAP]
sdb      8:16   0   20G  0 disk 
sdc      8:32   0   20G  0 disk 
sdd      8:48   0   20G  0 disk 
sr0     11:0    1   51M  0 rom  
~~~~

Ici on voit que sda est notre disque d'origine et les nouveaux
disques sont sdb, sdc, sdd.

On pourrait y créer directement des volumes physiques, je préfère
y créer une partition de type "LVM Linux" qui seront nos volumes
physique.

Trois fois :  `sudo cfdisk /dev/sdX` (ou X est b, c, d : vérifiez !)

- Type d'étiquette (i.e. type de table de partition) : GPT
- Nouvelle partition occupant tout l'espace
- Type : LVM Linux
- Écrire, valider avec oui ou yes
- Quitter

Vérifiez avec lsblk que tout semble ok :

~~~~Bash
$ lsblk 
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0   20G  0 disk 
├─sda1   8:1    0  512M  0 part /boot/efi
├─sda2   8:2    0 18,5G  0 part /
└─sda3   8:3    0  976M  0 part [SWAP]
sdb      8:16   0   20G  0 disk 
└─sdb1   8:17   0   20G  0 part 
sdc      8:32   0   20G  0 disk 
└─sdc1   8:33   0   20G  0 part 
sdd      8:48   0   20G  0 disk 
└─sdd1   8:49   0   20G  0 part 
sr0     11:0    1   51M  0 rom  
$ ls -l /dev/sd[bcd]*
brw-rw---- 1 root disk 8, 16 13 févr. 11:19 /dev/sdb
brw-rw---- 1 root disk 8, 17 13 févr. 11:19 /dev/sdb1
brw-rw---- 1 root disk 8, 32 13 févr. 11:20 /dev/sdc
brw-rw---- 1 root disk 8, 33 13 févr. 11:20 /dev/sdc1
brw-rw---- 1 root disk 8, 48 13 févr. 11:20 /dev/sdd
brw-rw---- 1 root disk 8, 49 13 févr. 11:20 /dev/sdd1
~~~~

L'initialisation des volumes physiques :

~~~~Bash
$ sudo pvcreate /dev/sd{b,c,d}1
  Physical volume "/dev/sdb1" successfully created.
  Physical volume "/dev/sdc1" successfully created.
  Physical volume "/dev/sdd1" successfully created.
$ sudo pvscan
$ sudo pvdisplay
~~~~

## Création d'un groupe de volumes

Créer un groupes de volumes nommé vg1 regroupant nos trois volumes
physiques :

~~~~Bash
$ sudo vgcreate vg1 /dev/sd{b,c,d}1
  Volume group "vg1" successfully created
$ sudo pvscan
  PV /dev/sdb1   VG vg1             lvm2 [<20,00 GiB / <20,00 GiB free]
  PV /dev/sdc1   VG vg1             lvm2 [<20,00 GiB / <20,00 GiB free]
  PV /dev/sdd1   VG vg1             lvm2 [<20,00 GiB / <20,00 GiB free]
  Total: 3 [<59,99 GiB] / in use: 3 [<59,99 GiB] / in no VG: 0 [0   ]
$ sudo vgscan
  Found volume group "vg1" using metadata type lvm2
$ sudo vgdisplay 
  --- Volume group ---
  VG Name               vg1
  System ID             
  Format                lvm2
  Metadata Areas        3
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                3
  Act PV                3
  VG Size               <59,99 GiB
  PE Size               4,00 MiB
  Total PE              15357
  Alloc PE / Size       0 / 0   
  Free  PE / Size       15357 / <59,99 GiB
  VG UUID               dzVxNt-D6bn-UD3k-Vp1p-epbc-hAdA-yW51A0
~~~~

## Création de deux volumes logiques

Objectifs : faire en sorte que `/var` soit à terme dans un volume
logique du groupe de volume `vg1` de 25Gio et que `/srv/data`
soit dans un volume logique occupant les 25Gio restant.

### Création des volumes logiques

~~~~Bash
$ sudo lvcreate --name var -L 25G vg1
  Logical volume "var" created.
$ lsblk 
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sdb           8:16   0   20G  0 disk 
└─sdb1        8:17   0   20G  0 part 
  └─vg1-var 254:0    0   25G  0 lvm  
sdc           8:32   0   20G  0 disk 
└─sdc1        8:33   0   20G  0 part 
  └─vg1-var 254:0    0   25G  0 lvm  
sdd           8:48   0   20G  0 disk 
└─sdd1        8:49   0   20G  0 part 
~~~~ 

On constate que le volume logique var est bien réparti sur sdb1 et sdc1 !

~~~~Bash
$ sudo lvcreate --name data -l 100%FREE vg1
  Logical volume "data" created.
jpierre@morlaix:~$ lsblk 
NAME         MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
...
sdb            8:16   0   20G  0 disk 
└─sdb1         8:17   0   20G  0 part 
  └─vg1-var  254:0    0   25G  0 lvm  
sdc            8:32   0   20G  0 disk 
└─sdc1         8:33   0   20G  0 part 
  ├─vg1-var  254:0    0   25G  0 lvm  
  └─vg1-data 254:1    0   35G  0 lvm  
sdd            8:48   0   20G  0 disk 
└─sdd1         8:49   0   20G  0 part 
  └─vg1-data 254:1    0   35G  0 lvm  
~~~~

var est réparti entre sdb1 et sdc1 et data entre sdc1 et sdd1.
Et nos périphériques blocs correpondant sont présent dans `/dev` :

~~~~Bash
$ ls -l /dev/vg1/
total 0
lrwxrwxrwx 1 root root 7 13 févr. 11:42 data -> ../dm-1
lrwxrwxrwx 1 root root 7 13 févr. 11:39 var -> ../dm-0
jpierre@morlaix:~$ ls -l /dev/mapper/ 
total 0
crw------- 1 root root 10, 236 13 févr. 11:39 control
lrwxrwxrwx 1 root root       7 13 févr. 11:42 vg1-data -> ../dm-1
lrwxrwxrwx 1 root root       7 13 févr. 11:39 vg1-var -> ../dm-0
~~~~

Objectif atteint : nos volumes logiques var et data sont prêts à être
"formatté" (accueillir des systèmes de fichiers qui pourront être
montés dans /var et /srv/data.

~~~~Bash
$ sudo lvscan 
  ACTIVE            '/dev/vg1/var' [25,00 GiB] inherit
  ACTIVE            '/dev/vg1/data' [<34,99 GiB] inherit
jpierre@morlaix:~$ sudo vgdisplay 
  --- Volume group ---
  VG Name               vg1
  System ID             
  Format                lvm2
  Metadata Areas        3
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               0
  Max PV                0
  Cur PV                3
  Act PV                3
  VG Size               <59,99 GiB
  PE Size               4,00 MiB
  Total PE              15357
  Alloc PE / Size       15357 / <59,99 GiB
  Free  PE / Size       0 / 0   
  VG UUID               dzVxNt-D6bn-UD3k-Vp1p-epbc-hAdA-yW51A0
~~~~

On vérifie que le volume logique var a une taille de 25Gio, data
environ 36Gio et que le groupe de volume vg1 n'a plus d'espace libre.

## Systèmes de fichiers

- État de l'art : ext4, xfs
- Intéressant : ZFS (NAS)
- Devrait être l'avenir : btrfs

Mise en place sur un périphérique bloc (partition ou volume logique) :
`mkfs.<type de fs>`

Objectifs : formater le volume logique var en ext4 et data en xfs.

~~~~Bash
$ sudo apt install xfsprogs
$ sudo mkfs.ext4 /dev/vg1/var
$ sudo mkfs.xfs  /dev/vg1/data
$ lsblk --fs
~~~~

### Montage automatique au démarrage des systèmes de fichiers

Dans `/etc/fstab` on ajoute (avant l'entrée du cdrom/dvd) deux
lignes :

~~~~
/dev/vg1/var   /var        ext4   defaults 0 2
/dev/vg1/data  /srv/data    xfs   defaults 0 2
~~~~

Reste à préparer les répertoire... Attention /var est pas vide !

En tout rigueur comme on va _déplacer_ `/var` on devrait passer en mode
rescue, mais le système est assez tolérant pour que ça passe.... et
on a de vrais service en production dessus...

~~~~Bash
$ sudo mv /var /var.old
$ sudo mkdir /var
$ sudo mkdir /srv/data
$ sudo mount -a
# si erreur ici : stop ! (warning systemd ok)
$ lsblk
$ df -h
$ sudo mv /var.old/* /var/
$ sudo reboot
~~~~

Attention en prod : ne faire ce genre de chose (déplacer un répertoire système) que
en mode "rescue".

## Redimensionnement à chaud de volumes logiques

Objectifs : Ajouter un volume physique de 200Gio à notre groupe de volumes vg1.

Étendre le volume logique var de 5Gio et data à 80% de l'espace disponibles dans le
groupe de volume de façon à ce que les systèmes de fichiers concernées changent
de taille à la volée.

Ajouter un disque de 200Gio dans Virtual Box.

Il est bien reconnu :

~~~~Bash
$ lsblk 
NAME         MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
...
sde            8:64   0  200G  0 disk 
$ sudo cfdisk /dev/sde  # crée une partition unique LVM Linux
$ sudo pvcreate /dev/sde1
$ sudo vgextend vg1 /dev/sde1
$ sudo vgdisplay
...
  Alloc PE / Size       15357 / <59,99 GiB
  Free  PE / Size       51199 / <200,00 GiB
~~~~

Étendre les volumes logiques concernés :

~~~~Bash
$ sudo lvextend /dev/vg1/var -L +5Gio --resizefs
$ sudo lvextend /dev/vg1/data -l +80%FREE --resizefs
$ df -h
$ sudo vgdisplay
...
  Free  PE / Size       9727 / <38,00 GiB
~~~~

Si vous oubliez `--resizefs` : vous pouvez exécuter 
plus tard `sudo resize2fs /dev/vg1/var` ou `xfs_growfs /dev/vg1/data`.

## Prise d'instantanés

Un instantané est un nouveau volume logique qui reflète l'état
d'un volume existant à un instant donné. Les différences qui
s'accumulent entre les deux (COW) sont stockés dans l'espace
que l'on alloue à ce volume instantané (en pratique 10-20%
de la taille du volume suffit). Une fois cette instantané
devenu inutile il faut le supprimer !

~~~~Bash
$ sudo mkdir /srv/data/$LOGNAME
$ ls /srv/data/
jpierre
$ sudo chown $LOGNAME:$LOGNAME /srv/data/$LOGNAME
$ cp /etc/fstab /srv/data/jpierre/

$ sudo lvcreate --snapshot --name data-snap -l 100%FREE /dev/vg1/data
~~~~

On peut monter ce snapshot et (par exemple) le sauvegarder, sans perturber
le volume d'origine :

~~~~Bash
$ sudo mount -o nouuid /dev/vg1/data-snap /mnt/
$ ls /mnt/
jpierre
$ ls /mnt/jpierre/
fstab
$ ls /srv/data/jpierre/
fstab
$ cp /etc/hosts /srv/data/jpierre/
$ ls /srv/data/jpierre/
fstab  hosts
$ ls /mnt/jpierre/
fstab
~~~~

Pour le supprimer (important !)

~~~~Bash
$ sudo umount /mnt
$ sudo lvremove /dev/vg1/data-snap
Do you really want to remove active logical volume vg1/data-snap? [y/n]: y
  Logical volume "data-snap" successfully removed
~~~~

