# Création de deux utilisateurs

avec adduser créer deux nompte joe et jane sur les deux systèmes

# bind mount


# Partage de fichiers avec NFS _(Network File System)_

Protocole créé par SUN Microsystems pour UNIX, versions 1, 2 puis 3.

NFSv4 est une réécriture complète plus simple (utilise tcp par défaut
et un seul numéro de port) et multi-plateforme (UNIX et MS Windows).

Côté serveur il faut install un paquet nfs-utils sous RHEL et
nfs-kernel-server sous Debian (il existe des alternative en
mode "user")

~~~~Bash
$ sudo apt install nfs-kernel-server
$ sudo vi /etc/exports
~~~~

Le fichier `/etc/exports` décrit les volumes NFS que nous allons
exporter. Note : en NFSv4 il faut que tous les partages soient
des sous-répertoires d'une base commune. Par exemple `/srv`.

~~~~
/srv         *(ro,no_subtree_check)
/srv/data    10.140.11.0/24(rw,no_subtree_check)
/srv/cnrs    10.140.11.0/24(rw,no_subtree_check)
# pour le système tunis seulement :
# correspond à /home : voir /etc/fstab
/srv/home    10.140.11.181(rw,no_subtree_check)
~~~~

On ajoute une ligne dans `/etc/fstab` afin que le contenu de `/home`
soit visible aussi dans `/srv/home` :

~~~~
/home           /srv/home inode bind 0 0
~~~~

Bon... On peut valider et relancer le serveur NFS :

~~~~Bash
$ sudo mkdir /srv/home
$ sudo mount -a
$ sudo systemctl restart nfs-kernel-server
$ sudo showmount -e
~~~~ 

Pour tester on va sur l'autre système (tunis) et on tente un
montage :

~~~~Bash
$ sudo showmount -e morlaix
$ sudo mount morlaix:/srv/home /mnt
$ ls /mnt
$ mount | grep nfs
$ sudo umount /mnt
~~~~

Check : /srv/home n'étant pas vide sur le serveur (Debian) on
doit voir quelque chose dans /mnt ci-dessus !!!

## Comment configurer le client pour que les montages NFS soient disponibles

On pourrait définir les montages réseau dans `/etc/fstab` :
:$

~~~~Bash
$ sudo mkdir -p /export/cnrs
$ sudo mkdir /export/data
~~~~

et ajouter dans fstab 

~~~~
morlaix:/srv/cnrs   /export/cnrs   nfs defaults 0 0
morlaix:/srv/data   /export/data  nfs defaults 0 0
~~~~

Mais c'est pas une bonne idée :

- Quid si le serveur NFS est indisponible quand le client démarre ?
- On charge le réseau pour rien si le partage n'est pas utilisé
- man fstab (options pour améliorer)

la bonne solution : montage automatique par l'automonteur à
configurer et installer sur le client

~~~~
$ sudo yum install autofs
~~~~

On édite `/etc/auto.master` (cartes) on ajoute :

~~~~
/export    auto.export
/home      auto.home
~~~~

On crée les fichier `/etc/auto.export`

~~~~
cnrs   morlaix:/srv/cnrs
data   morlaix:/srv/data
~~~~

et `/etc/auto.home`:

~~~~
jpierre :/srv/home/jpierre
joe     morlaix:/srv/home/joe
jane    morlaix:/srv/home/jane
...
~~~~

Ici on indique que le répertoire /home/jpierre reste local (on l'a
déplacé dans /srv ou /export au préalable) autofs fera un "bind mount", 
tandis que joe et jane ont leurs répertoires de connexion montés
en NFS à partir de morlaix automatiquement quand ils se connectent.

Donc sur le client on a fait avant :

~~~~Bash
$ sudo mkdir /srv/home
$ sudo mv /home/jpierre /srv/home/
$ sudo rm -rf /home/{joe,jane}
~~~~ 

On peut simplifier si on a beaucoup d'utilisateurs :

~~~~
jpierre :/srv/home/jpierre
*       morlaix:/srv/home/&
~~~~

Note : de telles "cartes" peuvent se stocker dans un annuaire
LDAP au lieu d'être définies sur chaque poste client. D'ailleurs
utiliser NFS pour /home avec un grand nombre d'utilisateurs
se fait généralement en utilisant un annuaire LDAP (ou _Active
Directory_) ainsi les comptes sont les mêmes partout
(logins, n° uid/gid, groupes, mots de passes, etc.)

~~~~
$ sudo systemctl restart autofs
$ ls -l /export
.. rien
$ ls -l /export/data
(pas d'erreur !!!)
$ ls -l /export
d.... data
(ouah ! magique !)
$ ls -l /home
$ ls -l /home/jpierre
$ ls -l /home/joe
~~~~ 

Les répertoires des utilisateurs sont montés automatiquement lors
d'une connexion (ssh, console, Gnome, etc.) :

~~~~
$ ssh joe@tunis
$ pwd
/home/joe
$ df .
morlaix:/srv/home/joe    19G    8,2G  9,1G  48% /home/joe
$ df ../jpierre
/dev/mapper/rl-root    17G    5,4G   12G  33% /home/jpierre
$ exit
~~~~

Notes : en plus de LDAP qui serait vraiment une conf prête 
pour la production, on peut sécuriser un peu plus NFS en
configuration un domaine _(realm)_ Kerberos. 

Remarque : Active Directory c'est DNS+LDAP+Kerberos+SMB+du bazar
et ont peut en faire sous Linux avec Samba 4 !


