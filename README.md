# Formation administration Debian GNU/Linux - Avril 2024

Liens :

https://debian-handbook.info/browse/fr-FR/stable/

https://framagit.org/jpython/meta

https://elixir.bootlin.com/linux/latest/source
https://bootlin.com/

Structure du initrd (TODO: plus zcat mais zstd -d ...)

https://wiki.debian.org/initramfs

Le site de Robert Cromwell : https://cromwell-intl.com/

https://www.brendangregg.com/perf.html

https://lwn.net/

## Compiler un module noyau

Le source est dans le dépôt : https://gitlab.com/python_431/debadm2402

Vous pouvez le cloner : `git cloner https://gitlab.com/python_431/debadm2402.git`
Si vous l'avez déjà cloné : aller dedans puis `git pull` 

Installer les fichiers d'en-tête et les Makefile adapté à notre noyau courant : 
`sudo apt install linux-headers-$(uname -r)` 

~~~~Bash
$ make
$ ls -l
$ sudo make install 
$ sudo modproble monmodule
$ lsmod | grep monmodule
$ sudo dmesg
...
Le module fubar est chargé...
$ sudo rmmod monmodule 
$ sudo dmesg
...
Module déchargé
~~~~

## Dépanner un système dont le mot de passe root est perdu

On interrompt GRUB, on sélectionne l'entrée exacte de notre démarrage et on édite l'entrée :

`ro quiet` -> `ro init=/bin/bash` (en QWERTY)

ctrl-x ou F10 pour démarrer ! ensuite dans le Shell (root) :

~~~~Bash
# mount -o rw,remount /
# passwd root
...
# mount -o ro,remount /
~~~~

On peut protéger l'édition via GRUB avec un mot de passe.

Pour des systèmes mobiles, la sécurisation repose sur le chiffrement du disque.

