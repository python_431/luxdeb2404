# Tunnel SSH sortant 

Scénario : vous avez un service accessible localement
sur un serveur, pas necesseraire publique (uniquement
sur localhost - 127.0.0.1 et ::1)

[ activer une seconde carte réseau dans VBox en NAT]

[ note : pour éviter un halt/poweroff sur la mauvaise
  machine : audo apt install molly-guard ]

ex : sur Debian 2 :
- installer Apache : sudo apt install apache2
- installer W3M : sudo apt install w3m

Vous pouvez voir le site Web par défaut d'Apache 2
sur Débian en mode console : w3m http://localhost/

Pour être reliste on va demander à Apache ne n'être
à l'écoute sur le port que sur localhost (127.0.01/::1) :

Dans /etc/apache2/ports.conf 

Listen 80 : remplacer par Listen localhost:80

et on relance : sudo systemctl restart apache2

http://locahost/ montre encore quelque chose
http://10.0.0.2/ sur Debian 1 : connexion refusée

Sur Debian 1 on n'a plus accès au service... Mais on
peut faire du ssh (sans mot de passe ou pas !), ssh
peut faire transiter le trafic de façon à ce que
le port local sur Debian 1 8080 soit acheminé sur
le port 80 de Debian 2

[ le client ssh va être serveur sur localhost:8080
et va rediriger le traffic via le serveur sshd qui
va être client vers le port 80 ]

~~~~
ssh -L *:8080:127.0.0.1:80 10.0.0.2 -f -N
~~~~

-L : tunnel sortant

* : toutes les interfaces (on peut mettre localhost)

127.0.0.1 : c'est du point de vue du sshd distant
 l'ip du serveur à contacter

10.0.0.2 : la machine qui fait tourner sshd

si vous allez sur http://localhost:8080/ sur Debian 1
vous accédez au service qui tourne sur Debian 2.

Le seul souci, en présence de NAT (sNAT de notre
côté ou du dNAT de l'autre côté) le tunnel va "tombé"
s'il n'y a pas de traffic (il faudra faire un kill
sur le process "ssh -L ..." et le relancer)

# Tunnel SSH entrant

Scénario : vous voulez accéder à un service
qui tourne sur votre station de
travail Linux (ou un serveur) sur lequel vous
avez un compte dans un réseau pas spécialement
ouvert de chez vous le week end, ce service peut
même être ssh lui-même !

Condition : de cette station vous pouvez faire
un ssh vers une machine sur l'Internet public
(dédié, cloud, etc..) où vous avez un compte.

~~~~Bash
$ ssh -R localhost:8080:127.0.0.1:80 user_id@srv_distant -f -N
~~~~

Donne accès au port 80 du sytème de départ sur le port
8080 local du srv_distant !

~~~~Bash
srv_distant:~$ w3m http://localhost:8080/
~~~~

Ça marche aussi pour le service SSH lui même !

~~~~Bash
$ ssh -R localhost:2222:127.0.0.1:22 user_id@srv_distant -f -N
~~~~

Alors on peut aller sur srv_distant et `ssh -p 2222 identifiant@localhost' donne accès à votre station !

