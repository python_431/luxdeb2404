# Déployer une applicatino Python avec Nginx/UWSGI


https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-22-04

## Installer et tester l'application

~~~~Bash
$ git clone https://framagit.org/jpython/share-code-plus.git
$ cd share-code-plus
$ sudo apt install python3-venv
$ python3 -m venv venv
$ ls venv
$ source venv/bin/activate
(venv) ... $ echo $PATH
.../venv/bin:...
$ pip install -r requirements.txt 
$ export FLASK_ENV=development
$ python sharecode.py
~~~~


## Déploiement

https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-22-04


