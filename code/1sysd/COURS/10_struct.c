#include<stdio.h>
#include<math.h>

typedef struct point point;
struct point {
	char *label;
	long double x,y;
};

void affiche_point(point p) {
	printf("%s est aux coordonnées (%.2Lf, %.2Lf)\n", p.label, p.x, p.y);
}

// à compléter : calcul de longueur du vecteur (O->p)
// (hint : racine carrée (sqrtl) de la somme des carrés des coordonnées
// man sqrt pour les détails

long double longueur(point p) {
    return sqrtl(p.x*p.x + p.y*p.y); 
}

int main() {
	point p1 = { "Le point A", 10.0, 3.14 };
	point p2;

	p2.label = "Le point B";
	p2.x = 42.0;
	p2.y = 12.1;

	affiche_point(p1);
	affiche_point(p2);
	printf("Distances à l'origine : %.2Lf %.2Lf\n", longueur(p1), longueur(p2));

	return 0;
}

