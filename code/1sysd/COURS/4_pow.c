#include<stdio.h>
#include<math.h>

int main() {
	double x, y;

	printf("Entrez deux nombre x et y : ");
	// lf : double (long float)
	// .2 : deux chiffres après le point décimal
	scanf("%lf %lf.2", &x, &y);
	printf("x^y = %lf\n", pow(x,y));
	return 0;
}
