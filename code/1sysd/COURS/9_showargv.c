#include<stdio.h>

// argc : arguments count (combien d'arguments)
// argv : arguments values
//
int main(int argc, char *argv[]) {
    printf("Nb d'arguments : %d\n", argc);
    for (int i = 0; i < argc; i++) {
        printf("%s\n", argv[i]);
    }
}
