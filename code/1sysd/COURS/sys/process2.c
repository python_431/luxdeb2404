#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>

int main() {
    int pid, me;
    int i;
    for (i = 0; i < 5; i++) {
       if ((pid = fork())) {
           // processus parent
           printf("Processus fils lancé n° %d: %d\n",i+1, pid);
       } else {
           // processus descendant
           me = getpid();
           printf("Ici le processus fils n° %d : mon PID est %d\n", i+1, me);
           printf("Fils dort 10s\n");
           sleep(10);
           printf("Child exiting...\n");
       }
    }
    printf("Parent dort 20s\n");
    sleep(20);
}
