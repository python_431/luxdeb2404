#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define ALIVE '*'
#define DEAD ' '

// Largeur et hauteur du monde
#define WIDTH  80
#define HEIGHT  25

// La grille du monde
char world[HEIGHT][WIDTH];

// Initialisation de la grille
void initWorld() {
  srand(time(NULL));
  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      world[i][j] = (rand() % 2 == 0)?DEAD:ALIVE;
    }
  }
}

// Afficher la grille
void displayWorld() {
  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      printf("%c", world[i][j]);
    }
    printf("\n");
  }
}

// Compter le nombre de voisins vivants autour de (i, j)
int countNeighbors(int i, int j) {
  int count = 0;
  for (int x = i - 1; x <= i + 1; x++) {
    for (int y = j - 1; y <= j + 1; y++) {
      if (x >= 0 && x < HEIGHT && y >= 0 && y < WIDTH) {
        if (world[x][y] == ALIVE && !(x == i && y == j)) {
          count++;
        }
      }
    }
  }
  return count;
}

// Mettre à jour la grille
void updateWorld() {
  char newWorld[HEIGHT][WIDTH];
  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      int count = countNeighbors(i, j);
      if (world[i][j] == ALIVE) {
        if (count == 2 || count == 3) {
          newWorld[i][j] = ALIVE;
        } else {
          newWorld[i][j] = DEAD;
        }
      } else {
        if (count == 3) {
          newWorld[i][j] = ALIVE;
        } else {
          newWorld[i][j] = DEAD;
        }
      }
    }
  }
  for (int i = 0; i < HEIGHT; i++) {
    for (int j = 0; j < WIDTH; j++) {
      world[i][j] = newWorld[i][j];
    }
  }
}

int main() {
  initWorld();

  // Mettre en place des formes initiales ici
  // ...

  while (1) {
    displayWorld();
    updateWorld();
    usleep(100000);
  }

  return 0;
}
