#include<stdio.h>
#include<stdlib.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);

    if (head == NULL) { // liste vide
        head = newnode;
    } else {            // on parcourt la liste jusqu'à la fin
        walk = head;
        while (walk->next != NULL) { // on va jusqu'au dernier nœud
            walk = walk->next;
        }
        walk->next = newnode; // on ajoute le nouvel élément
    }
    return head;
}

void append_val2(node **phead, int val) {
    node *newnode, **walk;
    newnode = create_node(val);

    walk = phead;
    while (*walk != NULL) {
        walk = &((*walk)->next);
    }
    *walk = newnode;
}


int main() {
    node *head = NULL;

    //head = append_val(head, 42);
    //head = append_val(head, 12);
    //head = append_val(head, -5);
    //head = append_val(head, 41);
    append_val2(&head, 42);
    append_val2(&head, 12);
    append_val2(&head, -5);
    append_val2(&head, 41);
    
    print_list(head);

}
