#include<stdio.h>

int main() {
     char txt1[50],txt2[50];
     char *p, *q;

     // chaîne identique ou pas ? 

     printf("Saisir deux mots : ");
     scanf("%s %s", txt1, txt2);

     p = txt1;
     q = txt2;
     // on stop si :
     // cas un : caractères différent (*p et *q) [txts différent]
     // p ou q sont arrivé (après incrémentation) sur une fin
     // de chaîne (0)
     //    deux cas : l'une des chaînes est terminé, pas l'autre [txt diff]
     //               zéro pour les deux [txt identique]
     while ( *p++ == *q++ && ( *p || *q ) );

     if ( *p == 0 && *q == 0 ) {
	printf("Identique.\n");
     } else {
        printf("Différent.\n");     
     }
}
