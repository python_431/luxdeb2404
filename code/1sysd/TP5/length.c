#include<stdio.h>

int main() {
    int i;
    char txt[100];
    char *p;

    printf("Entrez une chaîne : ");
    scanf("%[^\n]", txt);
    //scanf("%s", txt);
    printf("Txt : |%s|\n", txt);

    i = 0;
    while (txt[i]) {
	    printf("%i -> |%c| (%d)\n", i, txt[i], (int)txt[i]);
	    i++;
    }

    // plusieurs versions : 
    
    i = 0;
    while (txt[i] != 0) { // ou simplement txt[i]
	    i++;
    }
    printf("Longueur : %d\n", i);

    // notez : corps de la boucle : VIDE !
    // txt[i] est "faux" ssi char = 0
    for (i = 0; txt[i]; i++);
    printf("Longueur : %d\n", i);


    // avec arithmétique de pointeur :
    i = 0;
    p = txt;
    while (*p++) { i++; }
    printf("Longueur : %d\n", i);


}
