#include<stdio.h>
// Palindrome : première version avec des indices
// s[i] etc.

int main() {
	char s[50];
	int i, l;

	printf("Entrez un mot : ");
	scanf("%s", s);
	printf("Le mot : %s", s);

	for (l=0; s[l]; l++);
	printf(" a pour longueur %d\n", l);
	i=0;
	while (i*2 < l-1 && s[i] == s[l-i-1]) {
		printf("Indices %d et %d : comparaison de %c et %c\n", \
				i, l-i-1, s[i], s[l-i-1]);
		i++;
	}
	// si on sort arrivé au milieu : palindrome
	if (i*2 == l-1) { 
		printf("C'est un palindrome.\n");
	// sinon on est sorti parce que caractères différents
	// rencontrés
	} else {
		printf("Ce n'est pas un palindrome.\n");
	}
	return 0;

}
