#include<stdio.h>

void saisir_matrice(long double matrice[3][3]) {
    int i, j;
    printf("Saisir les éléments de la matrice :\n");
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            scanf("%Lf", &matrice[i][j]);
        }
    }
}

void afficher_matrice(long double matrice[3][3]) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%.2Lf ", matrice[i][j]);
        }
        printf("\n");
    }
}

int main() {
    long double matrice1[3][3] = {{1.1, 2.2, 3.3}, {4.4, 5.5, 6.6}, {7.7, 8.8, 9.9}};
    long double matrice2[3][3];

    saisir_matrice(matrice2);
    afficher_matrice(matrice1);
    afficher_matrice(matrice2);
    return 0;
}
