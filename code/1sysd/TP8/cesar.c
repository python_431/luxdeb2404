#include<stdio.h>

// Chiffrement avec l'algorithme de César
// en Python : chr( ( ord(c) - ord('a') + k ) % 26 + ord('a') )


void cesar(char *message, int key, char *crypted) {
    char *p,*q;
    // parcours de la chaîne
    // note: "," permet de séquencer deux instructions (ou plus)
    // là une seule est attendue (elles sont exécutées de gauche
    // à droite). On aura pu aussi faire q=crypted avant la 
    // boucle for et le q++ à la fin du corp de la boucle.
    for (p=message, q=crypted; *p; p++,q++) {
	    //printf("Traitement de %c\n", *p);
	    if (*p >= 'a' && *p <= 'z') {
		    *q = ( *p - 'a' + key ) % 26 + 'a';
	    } else {
		    *q = *p;
	    } 
    }   
    *q = '\0'; // ou 0 : fin de chaîne
}

int main() {
//    char message[] = "i do not like spam";
    char message[50];
    int secret;
    char crypted[50];

    printf("Message : ");
    scanf("%[^\n]", message);
    printf("Secret : ");
    scanf("%d", &secret);

    cesar(message, 5, crypted);
    printf("%s\n", crypted);
}
