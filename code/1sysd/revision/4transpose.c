#include<stdio.h>
#include<stdlib.h>

int main() {
    int tableau1[3][3] = { {1, 2, 3},
                           {4, 5, 6},
                           {7, 8, 9} };
    int i,j;
    int *p;

    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) { // pour transposer ce sera j<i
            printf("%d ", tableau1[i][j]); // ici ce sera
                                           // t[i][j] <-> t[j][i]
        }
        printf("\n");
    }
    // en fait c'est stocké à plat : i*(taille1) + j
    // for (i=0; i<9; i++) {
    //    printf("%d ", ((int *)tableau1)[i]);
    // }
}
