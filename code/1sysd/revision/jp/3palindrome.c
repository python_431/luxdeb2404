#include<stdlib.h>
#include<stdio.h>

// version simple : on inverse et on regarde
// si les deux chaînes sont égales (donc ~50% de comparaisons
// de caractères de trop)
// voir version 3palindrome_optimal.c pour une autre version

int palindrome(char *s) {
    char *p;
    char rev[50];
    int i, longueur = 0;
    // calcul de longueur
    p = s;
    while (*p++) { // ou *p++ != '\0' ou *p++ != 0
        longueur++;
    }
    // on copie à l'envers dans rev
    // 012345 (longueur est 5)
    // lapin0
    // nipal0
    for (i=0; i<longueur; i++) {
        rev[i] = s[longueur - i - 1];
    }
    // on ajoute le '\0' à la fin :
    rev[i] = 0; // ou '\0'
    // on compare s et rev
    p = rev;
    while ( *s == *p ) {
        s++;
        p++;
    }
    // si on arrive à la fin (des deux, même longueur)
    // c'est qu'elle sont égales
    return *s == '\0'; // ou 0
}

int main() {
    char saisie[50];

    printf("Un mot : ");
    scanf("%s", saisie);
    if (palindrome(saisie)) {
        printf("C'est un palindrome.\n");
    } else {
        printf("Ce n'est pas un palindrome.\n");
    }
}
