#include<stdio.h>
#include<stdlib.h>

//void transpose(int tab[][], int n) {
    int i,j,tmp;

    for (i=0; i<n; i++) {
        for (j=0; j<i; j++) { // de 0 à juste avant i 
            tmp = tab[i][j];    // inverse les valeurs en 
            tab[i][j] = tab[j][i];// [i][j] et [j][i]
            tab[j][i] = tmp;            
        }
    }
}

int main() {
    int tableau1[3][3] = { {1, 2, 3},
                           {4, 5, 6},
                           {7, 8, 9} };
    int tableau2[4][4] = { { 1,  2,  3,  4 },
                           { 5,  6,  7,  8 },
                           { 9, 10, 11, 12 },
                           {13, 14, 15, 16 } };
 
    int i,j,tmp;
    int *p;

    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) { // pour transposer ce sera j<i
            printf("%d ", tableau1[i][j]); // ici ce sera
                                           // t[i][j] <-> t[j][i]
        }
        printf("\n");
    }
    // en fait c'est stocké à plat : i*(taille1) + j
    // for (i=0; i<9; i++) {
    //    printf("%d ", ((int *)tableau1)[i]);
    // }
    transpose(tableau1, 3);
    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) { // pour transposer ce sera j<i
            printf("%d ", tableau1[i][j]); // ici ce sera
                                           // t[i][j] <-> t[j][i]
        }
        printf("\n");
    }
}
