## Révision

Livrable : un lien vers un dépôt GIT public (gitlab, github,
ou autre) ou bien une archive zip contenant les fichiers C
et un éventuel fichier `Makefile`.

1. fichier à fournir : `1pourcentage.c`

  créer deux fonctions attendant en arguments
  deux nombres de type float : prix et pourcentage 
  - une fonction augmente qui renvoie le prix augmenté du pourcentage
  - une fonction diminue sur le même principe
  - dans la fonction main, demander à l'utilisateur :
    - un caractère "+" ou "-"
    - un prix de type float
    - un pourcentage de type float
    - appelle la fonction adéquate et affiche le prix modifié

2. fichier à fournir : `2prefixe.c`

  Écrire une fonction longprefix qui attend deux chaînes de
  caractères et renvoie la longueur de leur préfixe commun :
   - "bonjour" et "bon" (ou "bon" et "bonjour") -> 3
   - "bonjour" et "au revoir" -> 0
  dans le main tester avec plusieurs exemples

3. `3palindrome.c`

  Écrire une fonction palindrome qui détermine si une chaîne est un
  palindrome comme "radar", "serres" ou non comme "lapin" (renvoie
  0 si pas un palindrome et différente de 9 si elle en est un)
  dans le programme principal tester en demandant une
  chaîne à l'utilisateur et en indiquant si la chaîne est
  un palindrome ou non.

4. `4transpose.c` (partir du fichier fourni)
  *STOP* pas si simple à faire avec une fonction

  Écrire une fonction transpose qui prend un tableau
  carré à deux dimensions de valeurs de type int ainsi
  que sa taille (nombre de lignes)
  et le transpose (i.e. transforme lignes en colonnes)

  exemple : le tableau 3x3 :

~~~~
    1 2 3
    4 5 6
    7 8 9
~~~~

  une fois transposé sera :

~~~~
    1 4 7
    2 5 8
    3 6 9 
~~~~

  tester avec ce tableau, puis avec un autre tableau
  4x4

5. `5intlist.c`
  
  On travaille sur une liste chaînée contruite sur la
  structure ainsi définie :

~~~~C
  typedef struct node node;
  struct node {
     int val;
     node *next;
  };
~~~~

  Écrire les fonctions :

  * `create_node(int val)`

   qui alloue dynamiquement une structure de type node,
   y stocke l'argument reçu, positionne next à NULL
   puis renvoie un pointeur vers cette donnée

  * `print_list(node *head)`

   qui affiche les nombres stockés sur une ligne et
   passe à la ligne ensuite

  * `append_val(node *head, int val)`

   qui crée un nœud contenant val et l'ajoute à la
   fin de la liste pointé par head et renvoie head
   (au cas où la liste était vide - valeur NULL -
    et donc le nœud placé en première position) et
    est donc utilisé ainsi :
    `head = append_val(head, 42)`

  * `append_val2(node **head, int val)`

   Même action que append_val mais 
   reçoit l'adresse d'un pointeur vers la liste 
      chaînée (et donc NULL si elle est vide)
      elle est donc utilisable ainsi : `append_val2(&head, 42)`

   Testez ces fonctions en créant une liste vide et
   en ajoutant plusieurs valeurs (avec les deux fonctions
   append_val et append_val2) et affiche la liste résultante

  * `intmax(head *node)`

   renvoie la valeur maximale de la liste, vous pouvez supposer
   que la liste reçue a au moins un élément.
  
