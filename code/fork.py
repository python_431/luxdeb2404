#!/usr/bin/env python3

from os import fork, wait
from time import sleep

pid = fork()
if pid:
    # pid > 0, je suis le parent
    print("Parent endormi jusqu'à la fin du descendant.")
    #pid, status = wait() # jusqu'à la fin du pid
    sleep(60)

    pid, status = wait() # jusqu'à la fin du pid
    print(f"Fin du fils : {pid}, status : {status}") 
    print("Find du parent.")
else:
    # pid est 0, je suis le descendant
    print("Descendant...", end="");
    for i in range(10):
        print(f"{i} ", end="", flush=True);
        sleep(1)
    print("Fin du processus fils.")

