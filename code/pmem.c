#include<stdlib.h>
#include<stdio.h>
#include<string.h>

int main() {
    char *text;
    char *msg = "Hello World\n";

    text = malloc(1024);

    strcpy(text, msg);

    printf(text); 

    // text = NULL;
    // no leak if this line uncommented:
    free(text);

    exit(0);

}
