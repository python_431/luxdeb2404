
#include<linux/module.h>
#include<linux/kernel.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("JP");
MODULE_DESCRIPTION("fubar");
MODULE_VERSION("Version 42");

int init_module(void) {
	printk(KERN_INFO "Le module fubar est chargé\n");
	return(0);
}

void cleanup_module(void) {
	printk(KERN_INFO "Goodbye...\n");
}
