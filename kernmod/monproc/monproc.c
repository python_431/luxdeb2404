#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>   
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#define BUFSIZE  100
 
 
MODULE_LICENSE("Dual BSD/GPL");
MODULE_AUTHOR("Liran B.H + JPM");
 
static int irq=20;
module_param(irq,int,0660);
 
static int mode=1;
module_param(mode,int,0660);
 
static struct proc_dir_entry *ent;
 
static ssize_t mywrite(struct file *file, const char __user *ubuf, size_t count, loff_t *ppos) 
{
 int num,c,i,m;
 char buf[BUFSIZE];
 if(*ppos > 0 || count > BUFSIZE)
  return -EFAULT;
 if(copy_from_user(buf, ubuf, count))
  return -EFAULT;
 num = sscanf(buf,"%d %d",&i,&m);
 if(num != 2)
  return -EFAULT;
 irq = i; 
 mode = m;
 c = strlen(buf);
 *ppos = c;
 return c;
}
 
static ssize_t myread(struct file *file, char __user *ubuf,size_t count, loff_t *ppos) 
{
 char buf[BUFSIZE];
 int len=0;
 static int num=0;
 num++;
 if(*ppos > 0 || count < BUFSIZE)
  return 0;
 len += sprintf(buf,"irq = %d\n",irq);
 len += sprintf(buf + len,"mode = %d\n",mode);
 len += sprintf(buf + len,"being read = %d\n",num);
 
 if(copy_to_user(ubuf,buf,len))
  return -EFAULT;
 *ppos = len;
 return len;
}
 
static struct proc_ops myops = 
{
 //.owner = THIS_MODULE,
 .proc_read = myread,
 .proc_write = mywrite,
};
 
static int simple_init(void)
{
 ent=proc_create("nantes",0660,NULL,&myops);
 printk(KERN_ALERT "hello...\n");
 return 0;
}
 
static void simple_cleanup(void)
{
 proc_remove(ent);
 printk(KERN_WARNING "bye ...\n");
}
 
module_init(simple_init);
module_exit(simple_cleanup);
